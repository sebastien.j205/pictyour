﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PictYour.Startup))]
namespace PictYour
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
